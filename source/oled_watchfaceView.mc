import Toybox.Application;
import Toybox.Graphics;
import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;
using Toybox.Time.Gregorian;
using Toybox.ActivityMonitor;

class oled_watchfaceView extends WatchUi.WatchFace {
    var sleeping = false;
    var lastDrawnMinute = 0;

    var width = 0;
    var height = 0;
    var size = 0;
    var center = [0,0];

    var colorHandSeconds = Graphics.COLOR_RED;
    var colorHandMinutes = Graphics.COLOR_GREEN;
    var colorHandHours = Graphics.COLOR_BLUE;
    var colorHandMiddleRingBase = Graphics.COLOR_WHITE;
    var colorHandMiddleRingWarning = Graphics.COLOR_RED;

    
    var colorBackgroudOutRing = Graphics.COLOR_WHITE;
    var colorBackgroudMiddleRing = Graphics.COLOR_DK_GRAY;
    var colorBackgroudInRing = Graphics.COLOR_WHITE;

    var imageHeart;


    function getHandCoors(angle, angleSplit, center, handSize, width) {
        angle /= 180.0;
        var size = width / 2.0;
        var angleSize = angleSplit / 180.0;
        var centerX = center[0];
        var centerY = center[1];
        var corAX = centerX + (Math.cos((angle + angleSize) * Math.PI) * size);
        var corAY = centerY + (Math.sin((angle + angleSize) * Math.PI) * size);
        var corBX = centerX + (Math.cos((angle + angleSize) * Math.PI) * (size - handSize));
        var corBY = centerY + (Math.sin((angle + angleSize) * Math.PI) * (size - handSize));
        var corCX = centerX + (Math.cos((angle - angleSize) * Math.PI) * (size - handSize));
        var corCY = centerY + (Math.sin((angle - angleSize) * Math.PI) * (size - handSize));
        var corDX = centerX + (Math.cos((angle - angleSize) * Math.PI) * size);
        var corDY = centerY + (Math.sin((angle - angleSize) * Math.PI) * size);
        
        return [
                [corAX, corAY],
                [corBX, corBY],
                [corCX, corCY],
                [corDX, corDY],
            ];
    }


    function initialize() {
        WatchFace.initialize();
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
        imageHeart = WatchUi.loadResource(Rez.Drawables.Heart);
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        if (width == 0 or height == 0) {
            width = dc.getWidth();
            height = dc.getHeight();
            size = width / 2.0;
            center = [size, size];
        }

        var clockTime = System.getClockTime();
        var hour = clockTime.hour;
        var minute = clockTime.min;
        if (sleeping) {
            var currentDrawnMinute = (hour * 60) + minute;
            if (currentDrawnMinute == lastDrawnMinute) {
                return;
            }
            lastDrawnMinute = currentDrawnMinute;
        }

        dc.clear();

        if (!sleeping) {
        // Get the current time and format it correctly
            var timeFormat = "$1$:$2$";
            var hours = clockTime.hour;
            if (!System.getDeviceSettings().is24Hour) {
                if (hours > 12) {
                    hours = hours - 12;
                }
            } else {
                if (Application.Properties.getValue("UseMilitaryFormat")) {
                    hours = hours.format("%02d");
                }
            }
            var timeString = Lang.format(timeFormat, [hours, clockTime.min.format("%02d")]);
            dc.setColor(getApp().getProperty("ForegroundColor") as Number, getApp().getProperty("BackgroundColor") as Number);
            dc.drawText(width / 2, height / 2, Graphics.FONT_SYSTEM_LARGE , timeString, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
        }

        //// DRAW HANDS

        var minuteAngle = (minute - 15.0) * 6.0;

        dc.setColor(colorHandHours, colorHandHours);
        var hourAngle = (hour - 3.0 + (minute / 60.0)) * 30.0;
        dc.fillPolygon(getHandCoors(hourAngle + 3, 1.8, center, 25, width));
        dc.fillPolygon(getHandCoors(hourAngle - 3, 1.8, center, 25, width));
        
        var cors = getHandCoors(minuteAngle, 2, center, 35, width);
        dc.setColor(colorHandMinutes, colorHandMinutes);
        dc.fillPolygon(cors);
        
        if (!sleeping) {
            dc.setColor(colorHandSeconds, colorHandSeconds);
            dc.fillPolygon(getHandCoors((clockTime.sec - 15.0) * 6.0, 1, center, 25, width));
        }

        ////


        // BATTERY
        // var batteryString = (System.getSystemStats().battery + 0.5).toNumber().toString();
        var sysStats = System.getSystemStats();
        var batteryAngle = 90 - (((sysStats.battery + 0.5) / 100) * 60);
        if (!sleeping or sysStats.battery < 10) {
            if (sysStats.battery < 10) {
                dc.setColor(colorHandMiddleRingWarning, colorBackgroudMiddleRing);
            } else if(sysStats.charging) {
                dc.setColor(Graphics.COLOR_GREEN, colorBackgroudMiddleRing);
            } else {
                dc.setColor(colorHandMiddleRingBase, colorBackgroudMiddleRing);
            }
            dc.fillPolygon(getHandCoors(batteryAngle, 1, center, 12, width-50));
        }


        var displayHeightScale = height / 218.0;

        // DATE
        if (!sleeping) {
            var today = Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);
            var todayString = Lang.format("$1$. $2$", [today.day, today.month]);
            dc.setColor(Graphics.COLOR_DK_GRAY, Graphics.COLOR_BLACK);
            dc.drawText(width / 2, height / 2 - (44 * displayHeightScale), Graphics.FONT_SYSTEM_SMALL , todayString, Graphics.TEXT_JUSTIFY_CENTER);
        } 


        // STEPS
        var activityInfo = ActivityMonitor.getInfo();
        dc.setPenWidth(5);

        var steps = activityInfo.steps;
        var stepGoal = activityInfo.stepGoal;
        var stepsInGoal = steps % stepGoal;
        var stepGoals = steps / stepGoal;
        var stepsAngleEnd = - 90 - ((60 * stepsInGoal) / stepGoal);
        var stepsAngle = -90;
        if (sleeping) {
            stepsAngle = stepsAngleEnd + 3;
        }
        dc.setColor(Graphics.COLOR_YELLOW, Graphics.COLOR_BLACK);
        if (stepsAngle != stepsAngleEnd) {
            dc.drawArc(center[0], center[0], size - 40, Graphics.ARC_CLOCKWISE, stepsAngle, stepsAngleEnd);
        }
        if (!sleeping) {
            dc.setColor(0xA0A030, Graphics.COLOR_BLACK);
            for (var i = 0; i < stepGoals; i++) {
                var start = -90 - (6 * i);
                dc.drawArc(center[0], center[0], size - 48, Graphics.ARC_CLOCKWISE, start, start - 4);
            }
        }

        // FLOORS
        var floors = activityInfo.floorsClimbed;
        var floorGoal = activityInfo.floorsClimbedGoal;
        var floorsInGoal = floors % floorGoal;
        var floorGoals = floors / floorGoal;
        var floorsAngleEnd = - 90 + ((60 * floorsInGoal) / floorGoal);
        var floorsAngle = -90;
        if (sleeping) {
            floorsAngle = floorsAngleEnd - 3;
        }
        dc.setColor(0x8530FF, Graphics.COLOR_BLACK);
        if (floorsAngle != floorsAngleEnd) {
            dc.drawArc(center[0], center[0], size - 40, Graphics.ARC_COUNTER_CLOCKWISE, floorsAngle, floorsAngleEnd);
        }
        if (!sleeping) {
            dc.setColor(0x4040C0, Graphics.COLOR_BLACK);
            for (var i = 0; i < floorGoals; i++) {
                var start = -90 + (6 * i);
                dc.drawArc(center[0], center[0], size - 48, Graphics.ARC_COUNTER_CLOCKWISE, start, start + 4);
            }
        }

        dc.setPenWidth(1);

        // HEART RATE
        var heartRate = Activity.getActivityInfo().currentHeartRate;
        // If heart rate is not available, this returns null. Be aware of this.
        if (heartRate != null) {
            if (!sleeping)  {
                dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_BLACK);
                dc.drawText(center[0], center[1] + 44, Graphics.FONT_TINY , heartRate.toString(), Graphics.TEXT_JUSTIFY_LEFT | Graphics.TEXT_JUSTIFY_VCENTER);
                dc.drawBitmap(center[0] - 20, center[1] + 34, imageHeart);
            } else if (heartRate > 90) {
                dc.drawBitmap(center[0], center[1] + 34, imageHeart);
            }
        }

    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
    }

    // The user has just looked at their watch. Timers and animations may be started here.
    function onExitSleep() as Void {
        sleeping = false;
    }

    // Terminate any active timers and prepare for slow updates.
    function onEnterSleep() as Void {
        sleeping = true;
        lastDrawnMinute = 0;
    }

}
