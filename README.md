Super simple watchface project with minimalistic UI.

## Explanation:
### Outer ring
- Red stripe is seconds hand
- Green stripe is minutes hand
- Blue stripe is hours hand

### Inner ring
- Right bottom corner is battery indicator
    - White is default
    - Red is when battery is under 10%
    - Green is when charging
    - Indicator at 6 o'clock means battery is emptry (0%)
    - Indicator at 4 o'clock means battery is full (100%)
- Yellow left bottom corner is steps indicator
    - Outer line is progress of the step goal or current multiplier of the step goal
        - 6 o'clock is 0 steps
        - 8 o'clock is full step goal
    - Inner line is step goal reached counter
- Purple right bootom corner is floors climbed indicator
    - Outer line is progress of the floor goal or current multiplier of the floor goal
        - 6 o'clock is 0 floors
        - 8 o'clock is full floor goal
    - Inner line is floor goal reached counter
- Green in middle is current time
- Gray above current time is current date
- Red under current time is current heart rate
    - When in sleep mode, heart rate is not displayed. Only when heart rate is over 90, heart icon is displayed.


## Images:
![Screenshot Awake](images/awake.png)
![Screenshot Asleep](images/sleep.png)


## Known issues:
- When putting the watch to sleep manualy, display stays black for a couple seconds. Reason is unknown.

## Notes & Todos:
- Try replace hands drawing with [`drawArc`](https://developer.garmin.com/connect-iq/api-docs/Toybox/Graphics/Dc.html#drawArc-instance_function)